/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.xo_game;

/**
 * 65160160 ฐานิพงศ์ สันติบวรวงศ์
 * 65160172 ธาม แก้วเกษ
 * 65160174 นพรัตน์ เพ็ชรศรี
 * 65160197 ศุภณัฐ อินทร์คง
 */
import java.util.Scanner;
import java.util.Random;

public class XO_game {

    public static void main(String[] args) {
        // form import
        Random rd = new Random();
        Scanner kb = new Scanner(System.in);

        // var
        char com = ' '; // รับpositionที่ได้รับจากการสุ่ม
        char position = ' ';// รับ ตำแหน่งจากผู้เล่น
        char answer = ' ';// รับคำตอบเริ่มใหม่หรือไม่
        
        char XO = ' ';
        char comXO = ' ';
        String a;

        System.out.println("Welcome To OX GAME!!!!");
        while (true) {
            int count = 1;
            // Board
            char[][] board = new char[3][3];
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    board[i][j] = Character.forDigit(count, 10);
                    count++;
                }
            }

            // outboard
           outBoard(board);

            // player choose X or O
            while (true) {
                System.out.print("Choose X or O: ");
                String input = kb.next();
                if (input.equalsIgnoreCase("X") || input.equalsIgnoreCase("O")) {
                    XO = input.toUpperCase().charAt(0);
                    break;
                }
                System.out.println("Invalid input. Please choose X or O.");
            }
            if (XO == 'X') {
                comXO = 'O';
            } else {
                comXO = 'X';
            }
            while (true) {
                // player turn
                System.out.print(XO + "  Turn Enter Number[1-9] : ");
                position = inPut(kb);
                // Change X/O in board
                changFinput(XO, position, board, kb);
                // outboard
                outBoard(board);
                // a คือ ฟังก์ชั่นเช็ค win ถ้าใช่ break ออกมาเลย
                a = CheckStatus.checkStatus(board);
                if (!a.equals("continue")) {
                    break;
                }

                // Random for progame
                while (true) {
                    int i = rd.nextInt(3);
                    int j = rd.nextInt(3);
                    if (board[i][j] != position && board[i][j] != 'X' && board[i][j] != 'O') {
                        com = board[i][j];
                        board[i][j] = comXO;
                        break;
                    }
                }
                System.out.println(comXO + " Turn : " + com);
                // outboard
                outBoard(board);
                // a คือ ฟังก์ชั่นเช็ค win ถ้าใช่ break ออกมาเลย
                a = CheckStatus.checkStatus(board);
                if (!a.equals("continue")) {
                    break;
                }  
            }
            if(a.equals("Draw!")){
                System.out.print("Game Over . Draw!");
            }else{
                System.out.println("Game Over. " + a+" Wins");
            }
            
            while (true) {
                System.out.print("Want to play again?  (Y or N) : ");
                String answeString = kb.next();
                if (answeString.equalsIgnoreCase("Y") || answeString.equalsIgnoreCase("N")) {
                    answer = answeString.toUpperCase().charAt(0);
                    break;
                }
                System.out.println("to start the game. Please choose Y or N.");
            }
            if (answer == 'N') {
                System.out.println("Thank you. Goodbye!");
                    break;
            }else{
                System.out.println("Welcome To OX GAME Again!!!!!!");
            }
           

        }

    }
    //function
    
    private static char inPut(Scanner kb){
        while (true) {
                        String input = kb.next();
                        if (input.length() == 1) {
                            char position = input.charAt(0);
                            if (position >= '1' && position <= '9') {
                                return position;
                            }
                        }
                        System.out.print("Invalid input. Please enter 1-9 : ");
                    }
        
    }
    
    private static void changFinput(char XO, char position, char[][] board, Scanner kb) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == position) {
                    board[i][j] = XO;
                    return;
                }
            }
        }
        System.out.print("Invalid position. Enter again :");
        char newPosition = inPut(kb);
        changFinput(XO, newPosition, board, kb);
     }
    
    private static void outBoard(char[][] board) {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(board[i][j] + " | ");
            }
            System.out.println();
            if (i < 2) {
                System.out.println("-----------");
            }
        }
    }


}
